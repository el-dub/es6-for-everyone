import { controls } from '../../constants/controls';

const pressed = new Set();

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    addEventListeners();
    setInterval(() => {
      checkPressed(firstFighter, secondFighter);
      if(!(firstFighter.health && secondFighter.health)) {
        const winner = firstFighter.health ? firstFighter : secondFighter;
        resolve(winner);
      }
    }, 20);
  });
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  if(blockPower >= hitPower) {
    return 0;
  }

  return hitPower - blockPower;
}

export function getHitPower(fighter) {
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  return fighter.defense * (Math.random() + 1);
}

let canDoCriticalHit = [true, true];

function doCriticalHit(fighter, fighterNum) {
  if(canDoCriticalHit[fighterNum]) {
    canDoCriticalHit[fighterNum] = false;
    setTimeout(() => {canDoCriticalHit[fighterNum] = true;}, 10000);
    return 2 * fighter.attack;
  }
  return 0;
}

function keysArePressed(...codes) {
  for (let code of codes) {
    if(!pressed.has(code)) {
      return false;
    }
  }

  return true;
}

function addEventListeners() {
  document.addEventListener('keydown', (event) => {
    pressed.add(event.code);
  });

  document.addEventListener('keyup', (event) => {
    pressed.delete(event.code);
  });
}

function checkPressed(firstFighter, secondFighter) {
  let damage;
  if(keysArePressed(controls.PlayerOneAttack) && !keysArePressed(controls.PlayerTwoBlock)) {
    damage = getDamage(firstFighter, secondFighter);
    decreaseHealth(1, secondFighter, damage);
    
    pressed.delete(controls.PlayerOneAttack);
  } else if(keysArePressed(controls.PlayerTwoAttack) && !keysArePressed(controls.PlayerOneBlock)) {
    damage = getDamage(secondFighter, firstFighter);
    decreaseHealth(0, firstFighter, damage);


    pressed.delete(controls.PlayerTwoAttack);
  } else if(keysArePressed(controls.PlayerOneCriticalHitCombination[0],
    controls.PlayerOneCriticalHitCombination[1],
    controls.PlayerOneCriticalHitCombination[2])) {
    damage = doCriticalHit(firstFighter, 0);
    decreaseHealth(1, secondFighter, damage);

    for(let control of controls.PlayerOneCriticalHitCombination) {
      pressed.delete(control);
    }
  } else if(keysArePressed(controls.PlayerTwoCriticalHitCombination[0],
    controls.PlayerTwoCriticalHitCombination[1],
    controls.PlayerTwoCriticalHitCombination[2])) {
    damage = doCriticalHit(secondFighter, 1);
    decreaseHealth(0, firstFighter, damage);

    for(let control of controls.PlayerTwoCriticalHitCombination) {
      pressed.delete(control);
    }
  }
  
}

function decreaseIndicator(fighterNumber, fighter, damage) {
  if(damage) {
    const indicatorId = fighterNumber ? 'right-fighter-indicator' : 'left-fighter-indicator';
    const indicator = document.getElementById(indicatorId);
    indicator.style.width = `${Math.round(((fighter.health - damage) * indicator.offsetWidth)/fighter.health)}px`;
  }
}

function decreaseHealth(fighterNumber, fighter, damage) {
  if(fighter.health - damage < 0) {
    decreaseIndicator(fighterNumber, fighter, fighter.health);
    fighter.health = 0;
  } else {
    decreaseIndicator(fighterNumber, fighter, damage);
    fighter.health -= damage;
  }
}