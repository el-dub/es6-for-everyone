import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  
  if(!fighter) {
    return fighterElement;
  }

  const imgElement = createFighterImage(fighter);
  fighterElement.append(imgElement);

  const fighterInfoElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___fighter-info',
  });

  const fighterName = createElement({ tagName: 'p', className: 'fighter-preview___fighter-info-text' });
  const fighterHealth = createElement({ tagName: 'p', className: 'fighter-preview___fighter-info-text' });
  const fighterAttack = createElement({ tagName: 'p', className: 'fighter-preview___fighter-info-text' });
  const fighterDefense = createElement({ tagName: 'p', className: 'fighter-preview___fighter-info-text' });

  fighterName.innerText = `Name: ${fighter.name}`;
  fighterHealth.innerText = `Health: ${fighter.health}`;
  fighterAttack.innerText = `Attack ${fighter.attack}`;
  fighterDefense.innerText = `Defense: ${fighter.defense}`;

  fighterInfoElement.append(fighterName, fighterHealth, fighterAttack, fighterDefense)

  fighterElement.append(fighterInfoElement);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
