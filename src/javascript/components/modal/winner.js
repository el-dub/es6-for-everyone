import { showModal } from './modal';

export function showWinnerModal(fighter) {
  showModal({
    title: 'Game over!',
    bodyElement: `Winner is ${fighter.name}`
  });
}
